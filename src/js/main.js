import '../scss/main.scss';

const form = document.querySelector('#form')
const inputNome = document.querySelector('.nome-input')
const inputEmail = document.querySelector('.email-input')
const textAreaMensagem = document.querySelector('.mensagem-ta')
const sucessToast = document.querySelector('.success-toast')
const btnScrollToTop = document.querySelector('.btn-scroll-to-top')

const showToast = () => {
    sucessToast.classList.toggle('show')

    setTimeout(() => {
        sucessToast.classList.remove('show')
    }, 2000)
}

const clearInputFields = () => {
    inputNome.value = ''
    inputEmail.value = ''
    textAreaMensagem.value = ''
}

const scrollToTop = () => {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}

form.addEventListener('submit', (e) => {
    e.preventDefault()
    clearInputFields()
    showToast()
})

btnScrollToTop.addEventListener('click', () => {
    scrollToTop()
})

