# Instruções


#### *É necessário que a máquina possua o Node e o NPM instalados

### 1 - Instale as dependências

Na raiz do projeto execute e aguarde as dependências serem baixadas:

```bash
npm install
```

### 2 - Execute o projeto com:

```bash
npm run dev
```

### 3 - No seu navegador acesse:

```bash
http://localhost:3000
```

### 4 - (Opcional) É possível gerar uma versão dist, executando:

```bash
npm run build
```

